import "./App.css";

import { Toaster } from "react-hot-toast";

import Routes from "./routes/Routes";

import { appRoutesConfiguration } from "./routes";

function App() {
  return (
    <>
      <Toaster position="top-left" reverseOrder={false} />
      <Routes mainRoutes={appRoutesConfiguration} isAuthorized={true} />
    </>
  );
}

export default App;
