import React from "react";
import { Route, Routes as ReactRoutes } from "react-router-dom";

import { generateFlattenRoutes } from "../utils/flattenDeepRoutes";

import NotFound from "../shared/NotFound";
import ProtectedRoute from "./ProtectedRoute";

const RenderRoutes = ({ mainRoutes, isAuthorized }) => {
  const layouts = mainRoutes.map(({ layout: Layout, routes }, index) => {
    const subRoutes = generateFlattenRoutes(routes);

    return (
      <Route key={index} element={<Layout />}>
        <Route
          element={
            <ProtectedRoute isPublic={true} isAuthorized={isAuthorized} />
          }
        >
          {subRoutes.map(({ component: Component, path, name, isPublic }) => {
            const routeIsPublic = isPublic !== undefined ? isPublic : true;
            return (
              Component &&
              routeIsPublic &&
              path && <Route key={name} element={<Component />} path={path} />
            );
          })}
        </Route>
      </Route>
    );
  });

  layouts.push(<Route key="not-found" path="*" element={<NotFound />} />);

  return <ReactRoutes>{layouts}</ReactRoutes>;
};

export default RenderRoutes;
