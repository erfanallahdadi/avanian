// pages
import HomePage from "../features/HomePgae/pages/HomePage";
import Register from "../features/Register/pages/Register";

// layouts
import AnonymousLayout from "../layouts/AnonymousLayout";
import MainLayout from "../layouts/MainLayout";

export const appRoutesConfiguration = [
  {
    layout: MainLayout,
    routes: [
      {
        name: "home",
        title: "Home page",
        component: HomePage,
        path: "/",
        isPublic: true,
      },
    ],
  },
  {
    layout: AnonymousLayout,
    routes: [
      {
        name: "register",
        title: "Register page",
        component: Register,
        path: "/register",
        isPublic: true,
      },
    ],
  },
];
