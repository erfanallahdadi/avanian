import React from "react";
import RenderRoutes from "./generate-routes";

const Routes = ({ mainRoutes, isAuthorized }) => {
  return <RenderRoutes mainRoutes={mainRoutes} isAuthorized={isAuthorized} />;
};

export default Routes;
