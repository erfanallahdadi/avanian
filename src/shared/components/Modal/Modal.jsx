import React, { useCallback, useEffect, useState } from "react";

function Modal(props) {
  const { children, isOpen = false, toggle, toggler, renderContent } = props;
  const [isVisible, setIsVisible] = useState(isOpen);

  useEffect(() => setIsVisible(isOpen), [isOpen]);

  const modalToggle = useCallback(() => {
    const newState = !isVisible;
    setIsVisible(newState);
    toggle?.(newState);
  }, [isVisible, toggle]);

  useEffect(() => {
    const handleClickOutside = (e) => {
      if (isVisible && e.target.classList.contains("modal-backdrop")) {
        modalToggle();
      }
    };

    document.addEventListener("click", handleClickOutside);

    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, [isVisible, modalToggle]);

  return (
    <div>
      {toggler && (
        <div onClick={modalToggle}>
          {typeof toggler === "function" ? toggler(isVisible) : toggler}
        </div>
      )}
      {isVisible && (
        <div className="modal-backdrop" onClick={modalToggle}>
          <div className="modal-content">
            {!renderContent && children}
            {typeof renderContent === "function"
              ? renderContent(isVisible, setIsVisible)
              : renderContent}
          </div>
        </div>
      )}
    </div>
  );
}

export default Modal;
