import { useEffect } from "react";

import "./BaseTab.css";

function BaseTab({ tabs, selectedTab, setSelectedTab }) {
  const handleTabClick = (tab) => {
    const clickedTab = tabs.find((item) => item.title === tab.title);
    setSelectedTab(clickedTab.title);
  };

  useEffect(() => {
    if (tabs?.length) {
      setSelectedTab(tabs[0].title);
    }
  }, []);

  if (!tabs?.length) {
    return (
      <div>
        <span>مقادیر منو وجود ندارد</span>
      </div>
    );
  }

  return (
    <div className="tab-container">
      {tabs.map((tab) => (
        <div
          className={`tab ${selectedTab === tab.title && "selected-tab"}`}
          onClick={() => handleTabClick(tab)}
          key={tab.id}
        >
          <span>{tab.title}</span>
        </div>
      ))}
    </div>
  );
}

export default BaseTab;
