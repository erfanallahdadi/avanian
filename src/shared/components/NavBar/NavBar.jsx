import React from "react";

import "./NavBar.css";
import { Link } from "react-router-dom";

function NavBar() {
  const items = [
    {
      id: 0,
      title: "گردشگری",
      address: "#newPage",
    },
    {
      id: 1,
      title: "فرهنگ و هنر",
      address: "#newPage",
    },
    {
      id: 3,
      title: "ورزش و سلامت",
      address: "#newPage",
    },
    {
      id: 4,
      title: "مهارت و سرگرمی",
      address: "#newPage",
    },
  ];
  const isAuthenticate = true;
  return (
    <div className="navbar-container">
      <div className="auth-buttons-container">
        <Link
          to={isAuthenticate ? "/register" : "/login"}
          className="avanian-button"
        >
          ثبتنام / ورود
        </Link>
      </div>
      <div className="item-container">
        {items.map((item) => (
          <a className="navbar-item" href={item.address} key={item.id}>
            {item.title}
          </a>
        ))}
      </div>
    </div>
  );
}

export default NavBar;
