import React from "react";
import arrow from "../../../assets/images/arrow.svg";

import "./CardModal.css";
function CardModal({ left, right, top, bottom, href, hasArrow }) {
  const containerStyle = {
    left,
    right,
    top,
    bottom,
  };
  return (
    <div className="card-container" style={containerStyle}>
      {hasArrow && (
        <a href={href} onClick={(e) => e.stopPropagation()}>
          <img className="arrow" src={arrow} alt="" />
        </a>
      )}
    </div>
  );
}

export default CardModal;
