import React from "react";
import { Outlet } from "react-router-dom";

const VODlayout = () => {
  return <Outlet />;
};

export default VODlayout;
