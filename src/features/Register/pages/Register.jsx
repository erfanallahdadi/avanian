import React, { useState } from "react";
import toast from "react-hot-toast";

import BaseTab from "../../../shared/components/BaseTab/BaseTab";

import avanianLogo from "../../../assets/images/avanianLogo.svg";
import zaniyar3 from "../../../assets/images/zaniyar/zaniyar3.svg";
import clouds from "../../../assets/images/clouds.svg";

import ParentRegisterForm from "../components/register/parentRegisterForm/ParentRegisterForm";
import ChildRegisterForm from "../components/register/childRegisterForm/ChildRegisterForm";

import "./register.css";

const tabs = [
  { id: 0, title: "ثبت نام والدین" },
  { id: 1, title: "ثبت نام فرزند" },
];

function Register() {
  const [selectedTab, setSelectedTab] = useState(null);
  const [parentRegisterFormData, setParentRegisterFormData] = useState({
    firstName: "",
    lastName: "",
    phoneNumber: "",
  });
  const [childRegisterFormData, setChildRegisterFormData] = useState({
    firstName: "",
    lastName: "",
    age: "",
    gender: "",
  });

  const resetParentForm = () => {
    setParentRegisterFormData({
      firstName: "",
      lastName: "",
      phoneNumber: "",
    });
  };

  const resetChildForm = () => {
    setChildRegisterFormData({
      firstName: "",
      lastName: "",
      age: "",
      gender: "",
    });
  };

  const submitHandler = (e) => {
    e.preventDefault();
    if (selectedTab === "ثبت نام والدین") {
      resetParentForm();
    } else {
      resetChildForm();
    }
    toast.error("در حال حاضر امکان ثبت نام وجود ندارد");
  };

  return (
    <div className="register-container">
      <img className="logo" src={avanianLogo} alt="" />
      <h2 className="register-title">ثبت نام</h2>
      <div>
        <div className="base-container">
          <BaseTab
            tabs={tabs}
            selectedTab={selectedTab}
            setSelectedTab={setSelectedTab}
          />
        </div>
        {selectedTab && selectedTab === "ثبت نام والدین" ? (
          <ParentRegisterForm
            onSubmit={submitHandler}
            formData={parentRegisterFormData}
            setFormData={setParentRegisterFormData}
          />
        ) : (
          <ChildRegisterForm
            onSubmit={submitHandler}
            formData={childRegisterFormData}
            setFormData={setChildRegisterFormData}
          />
        )}
      </div>
      {/* <img className="zaniyar" src={zaniyar3} alt="" />
      <img className="clouds" src={clouds} alt="" /> */}
    </div>
  );
}

export default Register;
