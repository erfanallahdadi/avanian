import React from "react";

import "../registerForm.css";
function ParentRegisterForm({ onSubmit, formData, setFormData }) {
  const changeHandler = (e) => {
    setFormData({
      [e.target.name]: e.target.value,
    });
  };

  return (
    <form className="form-container" onSubmit={onSubmit}>
      <div className="row">
        <div className="input-container">
          <input
            placeholder="نام"
            className="input-filed"
            type="text"
            name="firstName"
            value={formData.firstName}
            onChange={changeHandler}
          />
        </div>
        <div className="input-container">
          <input
            placeholder="نام خانوادگی"
            className="input-filed"
            type="text"
            name="lastName"
            value={formData.lastName}
            onChange={changeHandler}
          />
        </div>
      </div>
      <div className="row">
        <div className="input-container">
          <input
            placeholder="شماره تلفن"
            className="input-filed"
            type="text"
            name="phoneNumber"
            value={formData.phoneNumber}
            onChange={changeHandler}
          />
        </div>
      </div>
      <div className="row">
        <button type="submit" className="register-button">
          ثبت نام والد
        </button>
      </div>
    </form>
  );
}

export default ParentRegisterForm;
