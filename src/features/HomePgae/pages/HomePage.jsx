// HomePage.js
import React, { useRef, useEffect, useState } from "react";

import largeImage from "../../../assets/images/back.svg";
import largeImage2 from "../../../assets/images/back2.svg";

import NavBar from "../../../shared/components/NavBar/NavBar";
import ZaniyarSection from "../components/Zanyar/ZaniyarSection";
import PirouzSection from "../components/Pirouz/PirouzSection";
import DorsaSection from "../components/Dorsa/DorsaSection";

import "./HomePage.css";
import HomaSection from "../components/Homa/HomaSection";
import ArshamSection from "../components/Arsham/ArshamSection";
import Avanian from "../components/Avanian/Avanian";
import Cloud from "../components/Cloud/Cloud";

function HomePage() {
  const backgroundImageRef = useRef(null);
  const [modalCharState, setModalCharState] = useState({
    zaniyar: false,
    pirouz: false,
    dorsa: false,
    homa: false,
    arsham: false,
  });
  const [bgPosition, setBgPosition] = useState({
    x: 0,
    y: 0,
    width: 0,
    height: 0,
  });

  useEffect(() => {
    const calculateBgPosition = () => {
      if (backgroundImageRef.current) {
        const bgRect = backgroundImageRef.current.getBoundingClientRect();
        setBgPosition({
          x: bgRect.left + window.scrollX,
          y: bgRect.top + window.scrollY,
          width: bgRect.width,
          height: bgRect.height,
        });
      }
    };

    const handleLoad = () => {
      calculateBgPosition();
    };

    if (backgroundImageRef.current && !bgPosition.width && !bgPosition.height) {
      backgroundImageRef.current.addEventListener("load", handleLoad);
      calculateBgPosition();
    }

    const handleResize = () => {
      calculateBgPosition();
    };

    window.addEventListener("resize", handleResize);
    window.addEventListener("scroll", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
      window.removeEventListener("scroll", handleResize);
      backgroundImageRef.current?.removeEventListener("load", handleLoad);
    };
  }, []);

  // useEffect(() => {
  //   window.scroll(0, 0);
  //   // Remove hash when the page is reloaded
  //   if (window.location.hash) {
  //     window.history.replaceState(null, null, " ");
  //   }
  // }, []);

  return (
    <div
      className="container"
      onClick={() =>
        setModalCharState({
          zaniyar: false,
          pirouz: false,
          dorsa: false,
          homa: false,
          arsham: false,
        })
      }
    >
      <img
        ref={backgroundImageRef}
        src={largeImage}
        alt="Large Image"
        className="large-image"
      />
      <NavBar />
      {/* <Avanian bgPosition={bgPosition} />
      <Cloud bgPosition={bgPosition} /> */}
      <ZaniyarSection
        modalCharState={modalCharState}
        setModalCharState={setModalCharState}
        bgPosition={bgPosition}
      />
      <PirouzSection
        modalCharState={modalCharState}
        setModalCharState={setModalCharState}
        bgPosition={bgPosition}
      />
      <DorsaSection
        modalCharState={modalCharState}
        setModalCharState={setModalCharState}
        bgPosition={bgPosition}
      />
      <HomaSection
        modalCharState={modalCharState}
        setModalCharState={setModalCharState}
        bgPosition={bgPosition}
      />
      <ArshamSection
        modalCharState={modalCharState}
        setModalCharState={setModalCharState}
        bgPosition={bgPosition}
      />
    </div>
  );
}

export default HomePage;
