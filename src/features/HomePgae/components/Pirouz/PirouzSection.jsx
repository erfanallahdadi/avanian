import React, { useState } from "react";
import PropTypes from "prop-types";

import pirouz from "../../../../assets/images/pirouz/Pirouz1.svg";

import "./PirouzSection.css";
import Modal from "../../../../shared/components/Modal/Modal";
import CardModal from "../../../../shared/components/CardModal/CardModal";

function PirouzSection({ bgPosition, modalCharState, setModalCharState }) {
  const [animationStopped, setAnimationStopped] = useState(false);
  const { x, y, width: bgWidth, height: bgHeight } = bgPosition;

  const pirouzStyle = {
    top: `${y + bgHeight * 0.233}px`,
    left: `${x + bgWidth * 0.77}px`,
    width: `${bgWidth * 0.05}px`,
    height: "auto",
  };

  return (
    <div className="pirouz-section-container" id="pirouz" style={pirouzStyle}>
      <img
        className={`pirouz-img ${animationStopped ? "stop-animation" : ""}`}
        src={pirouz}
        alt=""
        onClick={(e) => {
          setModalCharState({
            zaniyar: false,
            pirouz: !modalCharState.pirouz,
            dorsa: false,
            homa: false,
            arsham: false,
          });
          e.stopPropagation();
          setAnimationStopped(true);
        }}
      />
      <Modal
        isOpen={modalCharState.pirouz}
        renderContent={(_isOpen, _setIsOpen) => (
          <CardModal right="100%" top="0" href="#dorsa" hasArrow={true} />
        )}
      />
    </div>
  );
}

PirouzSection.propTypes = {
  bgPosition: PropTypes.object.isRequired,
};

export default PirouzSection;
