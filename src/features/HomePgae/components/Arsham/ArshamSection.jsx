import React, { useState } from "react";
import PropTypes from "prop-types";

import arsham from "../../../../assets/images/arsham/Arsham1.svg";

import Modal from "../../../../shared/components/Modal/Modal";
import CardModal from "../../../../shared/components/CardModal/CardModal";

import "./ArshamSection.css";

function ArshamSection({ bgPosition, modalCharState, setModalCharState }) {
  const [animationStopped, setAnimationStopped] = useState(false);
  const { x, y, width: bgWidth, height: bgHeight } = bgPosition;

  const arshamStyle = {
    top: `${y + bgHeight * 0.45}px`,
    left: `${x + bgWidth * 0.57}px`,
    width: `${bgWidth * 0.07}px`,
    height: "auto",
  };

  return (
    <div className="arsham-section-container" id="arsham" style={arshamStyle}>
      <img
        className={`arsham-img ${animationStopped ? "stop-animation" : ""}`}
        src={arsham}
        alt=""
        onClick={(e) => {
          setModalCharState({
            zaniyar: false,
            pirouz: false,
            dorsa: false,
            homa: false,
            arsham: !modalCharState.arsham,
          });
          e.stopPropagation();
          setAnimationStopped(true);
        }}
      />
      <Modal
        isOpen={modalCharState.arsham}
        renderContent={(_isOpen, _setIsOpen) => (
          <CardModal right="100%" top="0" href="#pirouz" hasArrow={false} />
        )}
      />
    </div>
  );
}

ArshamSection.propTypes = {
  bgPosition: PropTypes.object.isRequired,
};

export default ArshamSection;
