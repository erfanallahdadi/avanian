import React from "react";

import avanianLogo from "../../../../assets/images/avanianLogo.svg";

import "./Avanian.css";
function Avanian({ bgPosition }) {
  const { x, y, width: bgWidth, height: bgHeight } = bgPosition;

  const avanianStyle = {
    top: `${y + bgHeight * 0.04}px`,
    right: `${x + bgWidth * 0.6}px`,
    width: `${bgWidth * 0.3}px`,
    height: "auto",
  };
  return (
    <div className="avanian-title" style={avanianStyle}>
      <img className="title-img" src={avanianLogo} alt="" />
    </div>
  );
}

export default Avanian;
