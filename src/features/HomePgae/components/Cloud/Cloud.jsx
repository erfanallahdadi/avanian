import React, { useEffect } from "react";

import clouds from "../../../../assets/images/clouds.svg";

import "./Cloud.css";
function Cloud({ bgPosition }) {
  const { x, y, width: bgWidth, height: bgHeight } = bgPosition;

  const cloudStyle = {
    top: `${y + bgHeight * 0.01}px`,
    right: `${x + bgWidth * 0.6}px`,
    width: `${bgWidth * 0.3}px`,
    height: "auto",
  };

  useEffect(() => {
    const interval = setInterval(() => {
      document.body.classList.toggle("animate");
    }, 5000);

    return () => clearInterval(interval);
  }, []);

  return (
    <div className="cloud-container" style={cloudStyle}>
      <img className="cloud-img" src={clouds} alt="" />
    </div>
  );
}

export default Cloud;
