import React, { useState } from "react";
import PropTypes from "prop-types";

import dorsa from "../../../../assets/images/dorsa/Dorsa1.svg";
import Modal from "../../../../shared/components/Modal/Modal";
import CardModal from "../../../../shared/components/CardModal/CardModal";

import "./DorsaSection.css";

function DorsaSection({ bgPosition, modalCharState, setModalCharState }) {
  const [animationStopped, setAnimationStopped] = useState(false);
  const { x, y, width: bgWidth, height: bgHeight } = bgPosition;

  const dorsaStyle = {
    top: `${y + bgHeight * 0.34}px`,
    left: `${x + bgWidth * 0.75}px`,
    width: `${bgWidth * 0.08}px`,
    height: "auto",
  };
  return (
    <div className="dorsa-section-container" id="dorsa" style={dorsaStyle}>
      <img
        className={`dorsa-img ${animationStopped ? "stop-animation" : ""}`}
        src={dorsa}
        alt=""
        onClick={(e) => {
          setModalCharState({
            zaniyar: false,
            pirouz: false,
            dorsa: !modalCharState.dorsa,
            homa: false,
            arsham: false,
          });
          e.stopPropagation();
          setAnimationStopped(true);
        }}
      />
      <Modal
        isOpen={modalCharState.dorsa}
        renderContent={(_isOpen, _setIsOpen) => (
          <CardModal right="100%" top="0" href="#homa" hasArrow={true} />
        )}
      />
    </div>
  );
}

DorsaSection.propTypes = {
  bgPosition: PropTypes.object.isRequired,
};

export default DorsaSection;
