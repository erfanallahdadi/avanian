import React, { useState } from "react";
import PropTypes from "prop-types";

import zaniyar from "../../../../assets/images/zaniyar/zaniyar1.svg";

import "./ZaniyarSection.css";
import Modal from "../../../../shared/components/Modal/Modal";
import CardModal from "../../../../shared/components/CardModal/CardModal";

function ZaniyarSection({ bgPosition, modalCharState, setModalCharState }) {
  const [animationStopped, setAnimationStopped] = useState(false);
  const { x, y, width: bgWidth, height: bgHeight } = bgPosition;

  const zaniyarStyle = {
    top: `${y + bgHeight * 0.088}px`,
    left: `${x + bgWidth * 0.54}px`,
    width: `${bgWidth * 0.1}px`,
    height: "auto",
  };

  return (
    <div className="zaniyar-section-container" style={zaniyarStyle}>
      <img
        className={`zaniyar-img ${animationStopped ? "stop-animation" : ""}`}
        src={zaniyar}
        alt=""
        onClick={(e) => {
          setModalCharState({
            zaniyar: !modalCharState.zaniyar,
            dorsa: false,
            homa: false,
            arsham: false,
          });
          e.stopPropagation();
          setAnimationStopped(true);
        }}
      />
      <Modal
        isOpen={modalCharState.zaniyar}
        renderContent={(_isOpen, _setIsOpen) => (
          <CardModal left="100%" bottom="-80%" href="#pirouz" hasArrow={true} />
        )}
      />
    </div>
  );
}

ZaniyarSection.propTypes = {
  bgPosition: PropTypes.object.isRequired,
};

export default ZaniyarSection;

// <div className="book">
// <h2 className="title">داستان آوانیان</h2>
// <p className="description">
//   به زمانی فکر کنید که کودک بودید و وانمود می‌کردید که یک پزشک، وکیل یا
//   ورزشکار حرفه‌ای هستید. با غوطه‌ور شدن در این نقش و کشف این نقش، متوجه
//   شده‌اید که این افراد چگونه رفتار می‌کنند و از طریق بازی یاد گرفته‌اند.
//   برنامه درسی مبتنی بر بازی به کودکان این آزادی را می‌دهد که در حین کشف
//   خود و جهان و علایق خود، سرگرم شوند.
// </p>
// </div>
