import React, { useState } from "react";
import PropTypes from "prop-types";

import homa from "../../../../assets/images/homa/Homa1.svg";

import "./HomaSection.css";
import Modal from "../../../../shared/components/Modal/Modal";
import CardModal from "../../../../shared/components/CardModal/CardModal";

function HomaSection({ bgPosition, modalCharState, setModalCharState }) {
  const [animationStopped, setAnimationStopped] = useState(false);
  const { x, y, width: bgWidth, height: bgHeight } = bgPosition;

  const homaStyle = {
    top: `${y + bgHeight * 0.36}px`,
    right: `${x + bgWidth * 0.73}px`,
    width: `${bgWidth * 0.08}px`,
    height: "auto",
  };

  return (
    <div className="homa-section-container" id="homa" style={homaStyle}>
      <img
        className={`homa-img ${animationStopped ? "stop-animation" : ""}`}
        src={homa}
        alt=""
        onClick={(e) => {
          setModalCharState({
            zaniyar: false,
            pirouz: false,
            dorsa: false,
            homa: !modalCharState.homa,
            arsham: false,
          });
          e.stopPropagation();
          setAnimationStopped(true);
        }}
      />
      <Modal
        isOpen={modalCharState.homa}
        renderContent={(_isOpen, _setIsOpen) => (
          <CardModal left="100%" top="0" href="#arsham" hasArrow={true} />
        )}
      />
    </div>
  );
}

HomaSection.propTypes = {
  bgPosition: PropTypes.object.isRequired,
};

export default HomaSection;
